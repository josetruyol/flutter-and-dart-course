import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Mi primera App'),
        ),
        body: Text("Este es mi texto por defecto!"),
      ),
    );
  }
}
